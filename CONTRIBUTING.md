If you wish to contribute feel free to make PRs, but if you aren't
as familiar with Git it is totally ok just to make an issue and ask
for this feature or that, or paste code in.